﻿<div class="bar">
    <a id="new" class="button" href="javascript:void(0);">Nuevo Cliente</a>
</div>
<table>
    <thead>
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Apellido</th>
			<th>TIpo de Usuario</th>	
            <th>Cantidad</th>
			<th>email</th>
            <th>Editar</th>
            <th>Borrar</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($view->usuarios as $usuario):  // uso la otra sintaxis de php para templates ?>
            <tr>
                <td><?php echo $usuario['id'];?></td>
                <td><?php echo $usuario['nombre'];?></td>
                <td><?php echo $usuario['apellido'];?></td>
				<td><?php echo $usuario['user_type_id'];?></td>
                <td><?php echo $usuario['cantidad'];?></td>
				<td><?php echo $usuario['email'];?></td>


                <td><a class="edit" href="javascript:void(0);" data-id="<?php echo $usuario['id'];?>">Editar</a></td>
                <td><a class="delete" href="javascript:void(0);" data-id="<?php echo $usuario['id'];?>">Borrar</a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

